import React from 'react'
import Navbar from './Components/Navbar'
import { BrowserRouter,Routes,Route } from 'react-router-dom'
import Homepage from './Pages/Homepage'
import AboutUs from './Pages/AboutUs'
import Register from './Pages/Register'
import ContactUs from './Pages/ContactUs'
import ProductsList from './Components/ProductsList';
import ProductMoreDetails from './Components/ProductMoreDetails';
import LoginPage from './Pages/LoginPage'
import Footer from './Components/Footer'



 function App(){
  return(
    <div>
      <BrowserRouter>
     <Navbar />
     <Routes>
     <Route path='/' element={<Homepage/>}/>
     <Route path='/contact' element={<ContactUs/>}/>
     <Route path='/about' element={<AboutUs/>}/>
     <Route path='/register' element={<Register/>}/>
     <Route path="/" element={<ProductsList />} />
     <Route path="/login" element={<LoginPage />} />
        <Route path="/moredetails" element={<ProductMoreDetails />} />
     </Routes>
     </BrowserRouter>
<Footer />
     
    </div>
  )}

export default App
