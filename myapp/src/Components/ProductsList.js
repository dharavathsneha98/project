import React, { useState } from 'react';
import { Card, CardBody, CardTitle, CardSubtitle, CardText } from 'reactstrap'; // Assuming you're using Reactstrap for cards
import { useNavigate } from 'react-router-dom';
import '../Pages/product.css'


function ProductsList() {
  const navigate = useNavigate();
 
  const [products, setProducts] = useState([
    {
      name: "Unexplored Destinations",
      subtitle: "Hidden Gems",
      imgsrc:"https://images.herzindagi.info/image/2022/Jul/places-to-travel-outside-india.jpg",
      items: [
        {Ladakh: 'Ladakh:NubraValley , Turtuk Village ,Hemis National Park Bali:Sidemen, Amed, Menjangan Island Pondicherry: Auroville , Paradise Beach , Gingee Fort Manali:Solang Valley , RohtangPass , OldManali Goa: ButterflyBeach, DivarIsland, ChoraoIsland Kerala: MarariBeach, Vagamon, BekalFort '},
      ]
    },
    {
      name: "Thrilling Experiences",
      subtitle: "Adventure Activities",
      imgsrc:"https://imgcld.yatra.com/ytimages/image/upload/t_ann_seo_banner_xlarge/v1530015312/AdvNation/ANN_TRP156/adventure-trips-in-india_1440486703_aNbqdJ.jpg",
      items: [
        {Ladakh: 'Ladakh: Trekking in Markha Valley, River Rafting in Zanskar River'},
          { Bali: 'Bali: Surfing in Kuta, White Water Rafting in Ayung River'},
         { Pondicherry: 'Pondicherry: Scuba Diving in Pondicherry, Surfing in Auroville'},
          { Manali: 'Manali: Paragliding in Solang Valley, Skiing in Solang Nallah'},
         { Goa: 'Goa: Jet Skiing in Calangute, Parasailing in Baga Beach'},
          {Kerala: 'Kerala: Houseboat Stay in Alleppey, Bamboo Rafting in Periyar National Park'},
       
      ]
    },
    {
      name: " Exploring Traditions",
      subtitle: "Cultural Delights",
      imgsrc: "https://www.holidify.com/blog/wp-content/uploads/2015/07/13896645072_c994520879_k.jpg",
      items: [
      {Ladakh: 'Ladakh: Hemis Festival, Ladakhi Cuisine, Monasteries'},
      {Bali:'Bali: Balinese Dance Performances, Traditional Temples, Gamelan Music'},
      {Pondicherry: 'Ponducherry: French Quarter, Aurobindo Ashram, Tamil Heritage'},
      {Manali: 'Manali: Himachali Folk Dance, Tibetan Monasteries, Local Cuisine'},
      {Goa: 'Goa: Portuguese Influence, Goan Carnival, Fado Music'},
      { Kerala: 'Kerala: Kathakali Dance, Backwater Village Life, Ayurvedic Traditions'},
      ]
    },
    {
      name: "Must-Try Cuisines",
      subtitle: "Foodie's Paradise",
      imgsrc: "https://www.tastingtable.com/img/gallery/20-delicious-indian-dishes-you-have-to-try-at-least-once/intro-1645057933.webp", 
      items: [
        
 {Ladakh: 'Ladakh: Momos, Thukpa, Butter Tea'},
 { Bali: 'Bali: Nasi Goreng, Babi Guling, Balinese Satay'},
 {Pondicherry: 'Pondicherry: French Crepes, Seafood, Filter Coffee'},
 {Manali: 'Manali: Himachali Dham, Siddu, Trout Fish'},
 
      ]
    },
    {
      name: "Scenic Spots",
      subtitle: "Historical Marvels",
      imgsrc:"https://www.tourmyindia.com/blog//wp-content/uploads/2016/05/Historical-Places-in-Central-India-that-You-Must-Visit.jpg",
      items: [
        
{Ladakh:
  'Ladakh: Leh Palace A former royal palace with stunning architecture and panoramic views.Alchi Monastery Known for its ancient wall paintings and intricate woodwork. Basgo Monastery A 15th-century monastery with impressive murals and statues.'},
  
  {Bali:
  'Bali: Tanah Lot Temple: A beautiful sea temple perched on a rocky outcrop.Besakih Temple Known as the "Mother Temple," it is the largest and holiest Hindu temple in Bali. Tirta Empul Temple Famous for its sacred spring water and purification rituals.'},
  
  { Pondicherry:
  'Pondicherry: Arikamedu An ancient Roman trading post with remnants of pottery and beads. Gingee Fort A majestic fortress known for its intricate architecture and historical significance. French War Memorial A memorial commemorating the soldiers who lost their lives in World War I.'},
  
  { Manali:
   'Manali: Hadimba Temple: A wooden temple dedicated to the goddess Hadimba, known for its unique architecture. Naggar Castle A historic castle-turned-heritage hotel, showcasing traditional Himachali architecture. Manu Temple A peaceful temple dedicated to Sage Manu, believed to be the creator of human race.'},
  
  {Goa:
  'Goa: Fort Aguada A well-preserved 17th-century Portuguese fort with a lighthouse and stunning viewsBasilica of Bom Jesus A UNESCO World Heritage site housing the remains of St. Francis Xavier.Se Cathedral One of the largest churches in Asia, known for its Portuguese-Gothic architecture.'},
  
   {Kerala:
  'Kerala: Mattancherry Palace A Portuguese palace known for its exquisite murals and traditional Kerala architecture. Bekal Fort A massive fort overlooking the Arabian Sea, offering breathtaking views. Jewish Synagogue One of the oldest synagogues in the Commonwealth of Nations, adorned with beautiful chandeliers and hand-painted tiles.'},
      ]
    },
   
  ]);

  const displayMoreDetails = (index) => {
    navigate("/moredetails", { state: { selectedIndex: index } });
  }

  return (
    <div className='d-flex flex-wrap justify-content-center mt-4'>
      {products && products.map((item, index) => (
        <div key={index}>
          <Card style={{ width: '18rem' }} className='m-2'>
            <CardBody>
              <CardTitle tag="h5">{item.name}</CardTitle>
              <CardSubtitle className="mb-2 text-muted" tag="h6">{item.subtitle}</CardSubtitle>
            </CardBody>
            <img alt="Card cap" src={item.imgsrc} style={{width:"100%",height:"40vh"}} />
            <CardBody>
              <CardText>
              Here are a few topics on tourism for Ladakh, Bali, Pondicherry, Manali, Goa, and Kerala
              </CardText>
              <button onClick={() => displayMoreDetails(index)}>View More</button>
              
            </CardBody>
          </Card>
        </div>
      ))}
    </div>
  );
}

export default ProductsList;
