import React from 'react';
import { useLocation } from 'react-router-dom';
import '../Pages/product.css'

const ProductMoreDetails = () => {
  const location = useLocation();
  const selectedIndex = location.state ? location.state.selectedIndex : null;

  if (selectedIndex === null) {
    return <div>No product selected</div>;
  }

  const products = [
    {
      name: 'Unexplored Destinations',
      subtitle: 'Hidden Gems',
     // imgsrc: "https://images.herzindagi.info/image/2022/Jul/places-to-travel-outside-india.jpg",
      items: [
        {Ladakh: ' Ladakh: NubraValley , Turtuk Village ,Hemis National Park' },
        { Bali: 'Bali: Sidemen, Amed, Menjangan Island'},
        { Pondicherry: ' Pondicherry:  Auroville , Paradise Beach , Gingee Fort'},
         {Manali: 'Manali: Solang Valley , RohtangPass , OldManali'},
         {Goa: 'Goa: ButterflyBeach, DivarIsland, ChoraoIsland'},
         {Kerala: 'Kerala: MarariBeach, Vagamon, BekalFort '},
       
        
      ]
    },
    {
      name: 'Thrilling Experiences',
      subtitle: 'Adventure Activities',
      //imgsrc: "https://imgcld.yatra.com/ytimages/image/upload/t_ann_seo_banner_xlarge/v1530015312/AdvNation/ANN_TRP156/adventure-trips-in-india_1440486703_aNbqdJ.jpg",
      items: [
        {Ladakh: 'Ladkh:Trekking in Markha Valley, River Rafting in Zanskar River'},
          { Bali: 'Bali:Surfing in Kuta, White Water Rafting in Ayung River'},
         { Pondicherry: 'Pondicherry:Scuba Diving in Pondicherry, Surfing in Auroville'},
          { Manali: 'Manali:Paragliding in Solang Valley, Skiing in Solang Nallah'},
         { Goa: 'Goa:Jet Skiing in Calangute, Parasailing in Baga Beach'},
          {Kerala: 'Kerala:Houseboat Stay in Alleppey, Bamboo Rafting in Periyar National Park'},
        
        
      ]
    },
    {
      name: 'Exploring Traditions ',
      subtitle: 'Cultural Delights',
      //imgsrc: '"https://www.holidify.com/blog/wp-content/uploads/2015/07/13896645072_c994520879_k.jpg" ',
      items: [
        {Ladakh: 'Ladakh:Hemis Festival, Ladakhi Cuisine, Monasteries'},
        {Bali:'Bali: Balinese Dance Performances, Traditional Temples, Gamelan Music'},
        {Pondicherry: 'Pondicherry:French Quarter, Aurobindo Ashram, Tamil Heritage'},
        {Manali: 'Manali:Himachali Folk Dance, Tibetan Monasteries, Local Cuisine'},
        {Goa: 'Goa:Portuguese Influence, Goan Carnival, Fado Music'},
        { Kerala: 'Kerala:Kathakali Dance, Backwater Village Life, Ayurvedic Traditions'},
        
      ]
    },
    {
      name: 'Must-Try Cuisines ',
      subtitle: 'Foodies Paradise',
      //imgsrc:'https://www.tastingtable.com/img/gallery/20-delicious-indian-dishes-you-have-to-try-at-least-once/intro-1645057933.webp"',
      items: [
       
        {Ladakh: 'Ladakh:Momos, Thukpa, Butter Tea'},
        { Bali: 'Bali:Nasi Goreng, Babi Guling, Balinese Satay'},
        {Pondicherry: 'Pondicherry:French Crepes, Seafood, Filter Coffee'},
        {Manali: 'Manali:Himachali Dham, Siddu, Trout Fish'},
        
        
      ]
    },
    {
      name: 'Exploring Heritage Sites',
      subtitle: 'Historical Marvels',
      //imgsrc: "https://www.tourmyindia.com/blog//wp-content/uploads/2016/05/Historical-Places-in-Central-India-that-You-Must-Visit.jpg" ,
      items: [
               
{Ladakh:
  'Ladakh:Leh Palace A former royal palace with stunning architecture and panoramic views.Alchi Monastery Known for its ancient wall paintings and intricate woodwork. Basgo Monastery A 15th-century monastery with impressive murals and statues.'},
  
  {Bali:
  'Bali:Tanah Lot Temple: A beautiful sea temple perched on a rocky outcrop.Besakih Temple Known as the "Mother Temple," it is the largest and holiest Hindu temple in Bali. Tirta Empul Temple Famous for its sacred spring water and purification rituals.'},
  
  { Pondicherry:
  'Pondicherry:Arikamedu An ancient Roman trading post with remnants of pottery and beads. Gingee Fort A majestic fortress known for its intricate architecture and historical significance. French War Memorial A memorial commemorating the soldiers who lost their lives in World War I.'},
  
  { Manali:
   'Manali:Hadimba Temple: A wooden temple dedicated to the goddess Hadimba, known for its unique architecture. Naggar Castle A historic castle-turned-heritage hotel, showcasing traditional Himachali architecture. Manu Temple A peaceful temple dedicated to Sage Manu, believed to be the creator of human race.'},
  
  {Goa:
  'Goa:Fort Aguada A well-preserved 17th-century Portuguese fort with a lighthouse and stunning viewsBasilica of Bom Jesus A UNESCO World Heritage site housing the remains of St. Francis Xavier.Se Cathedral One of the largest churches in Asia, known for its Portuguese-Gothic architecture.'},
  
   {Kerala:
  'Kerala:Mattancherry Palace A Portuguese palace known for its exquisite murals and traditional Kerala architecture. Bekal Fort A massive fort overlooking the Arabian Sea, offering breathtaking views. Jewish Synagogue One of the oldest synagogues in the Commonwealth of Nations, adorned with beautiful chandeliers and hand-painted tiles.'},
        
      ]
    },
    
  ];

  const selectedProduct = products[selectedIndex];

  return (
    <div>
      <h1>{selectedProduct.name}</h1>
      <div className="product-container">
        <div className="product-image">
          
        </div>
        <div className="product-details">
          {selectedProduct.items.map((item, index) => (
            <div key={index} className="item">
              <h5>{item.Ladakh}</h5>
              <h5>{item.Bali}</h5>
              <h5>{item.Pondicherry}</h5>
              <h5>{item.Manali}</h5>
              <h5>{item.Goa}</h5>
              <h5>{item.Kerala}</h5>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}

export default ProductMoreDetails;
