import React from 'react'
import { Link } from 'react-router-dom'

function Navbar() {
    const navigationstyles={
        backgroundColor:'black',
        color:'white',
        margin:'0px',
        display:'flex',
        flexWrap:'wrap',
        padding:'10px',
        justifyContent:'space-between'
    }
    const links={
        listStyleType:'none',
        cursor:'pointer',
        margin:'0 10px'
    }
  return (
    <div style={navigationstyles}>
       
        <h1 style={{justifyContent:'center'}}>Panchhi Bano</h1>
        <div >
            <ol className='d-flex m-2'>
                <div style={{display:'flex', flexWrap:'wrap',justifyContent:'space-evenly',}}>
                <li style={links} className='p-1'><Link to='/' style={{textDecoration:'none', color:'white' }}>HomePage</Link></li>
                <li style={links} className='p-1'><Link to='/about' style={{textDecoration:'none',color:'white'}}>About us</Link></li>
                <li style={links} className='p-1'><Link to='/contact' style={{textDecoration:'none',color:'white'}}>Contact us</Link></li>
                <li style={links} className='p-1'><Link to='/register' style={{textDecoration:'none',color:'white'}}>Register</Link></li>
                <li style={links} className='p-1'><Link to='/login' style={{textDecoration:'none',color:'white'}}>Login</Link></li>
                </div>
            </ol>
        </div>
      
    </div>
  )
}










export default Navbar