import React from 'react';
import travelImage from './images/travel.avif';
import './About.css';

const AboutUs = () => {
  return (
    <div className="about-container">
      {/* Left image */}
      <div className='item1'>
        <img src={travelImage} alt="Manali" className="left-image" />
      </div>
      <div className='item2'>
        <div className="content">
          <h2>About Panchibano</h2>
          <p>Welcome to Panchibano, your go-to destination for affordable and unforgettable travel experiences! At Panchibano, we believe that everyone deserves the chance to explore the beauty of our world without breaking the bank. That's why we're dedicated to curating budget-friendly tours to some of the most captivating destinations across India.</p>
        </div>
        {/* Add your content here */}
        <div className="content">
          <h3>Our Mission</h3>
          <p>Our mission at Panchibano is simple: to make travel accessible and enjoyable for all. We understand the thrill of embarking on a new adventure, and we're committed to providing you with hassle-free travel experiences that won't drain your wallet.</p>
        </div>
        <div className="content">
          <h3>What We Offer</h3>
          <ul>
            <li><strong>Budget-Friendly Tours:</strong> We specialize in crafting affordable travel packages that cater to your budget constraints without compromising on quality or comfort.</li>
            <li><strong>Expert Guidance:</strong> Our team of experienced travel experts is here to assist you every step of the way, from planning your itinerary to ensuring a smooth and seamless journey.</li>
            <li><strong>Unforgettable Experiences:</strong> Whether you're craving a tranquil retreat amidst the serene backwaters of Kerala or seeking adrenaline-pumping adventures in the rugged landscapes of Ladakh, we've got you covered.</li>
          </ul>
        </div>
        <div className="content">
          <h3>Destinations</h3>
          <h4>Manali</h4>
          <p>Surrounded by towering snow-capped peaks and lush pine forests, Manali is a charming hill station nestled in the lap of the Himalayas. Explore scenic valleys, indulge in adventurous activities like trekking and skiing, and discover the rich cultural heritage of this enchanting destination.</p>
          <h4>Goa</h4>
          <p>Famous for its sun-kissed beaches, vibrant nightlife, and Portuguese-influenced architecture, Goa is a paradise for beach lovers and party enthusiasts alike. Relax on pristine beaches, savor delicious seafood cuisine, and immerse yourself in the laid-back coastal vibes of this tropical haven.</p>
          <h4>Kerala</h4>
          <p>Known as "God's Own Country," Kerala boasts a diverse landscape of tranquil backwaters, lush hill stations, and palm-fringed beaches. Cruise along the serene Alleppey backwaters, explore the tea plantations of Munnar, and rejuvenate your senses with Ayurvedic treatments amidst the natural beauty of this picturesque state.</p>
          <h4>Pondicherry</h4>
          <p>With its charming French colonial architecture, vibrant markets, and serene beaches, Pondicherry exudes a unique blend of Indian and European influences. Explore the cobblestone streets of the French Quarter, relax on pristine beaches like Paradise Beach, and immerse yourself in the spiritual ambiance of Auroville.</p>
          <h4>Ladakh</h4>
          <p>Nestled amidst the rugged landscapes of the Himalayas, Ladakh is a land of breathtaking beauty and cultural richness. Marvel at the surreal landscapes of Pangong Lake, explore ancient monasteries like Thiksey Monastery, and embark on thrilling adventures like white-water rafting and trekking in this remote Himalayan region.</p>
          <h4>Bali</h4>
          <p>Known as the "Island of the Gods," Bali is a tropical paradise nestled in the Indonesian archipelago. Renowned for its stunning beaches, lush rice terraces, and vibrant cultural scene, Bali offers a diverse array of experiences for travelers. Explore ancient temples such as Tanah Lot and Uluwatu, unwind on pristine beaches like Kuta and Seminyak, and immerse yourself in the island's rich artistic heritage through traditional dance performances and local handicrafts. Whether you're seeking relaxation, adventure, or cultural exploration, Bali is sure to captivate your senses and leave you with unforgettable memories.</p>

        
        </div>
        <div className="content">
          <h3>Get in Touch</h3>
          <p>Ready to embark on your next adventure? Contact us today to start planning your dream getaway with Panchibano!</p>
        </div>
      </div>
    </div>
  );
};

export default AboutUs;
