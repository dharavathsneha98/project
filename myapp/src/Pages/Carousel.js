import React from 'react';
import Slider from 'react-slick';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import keralaImage from './images/kerala.jpg';
import manaliImage from './images/manaliii.jpg';
import ladakhImage from './images/ladakh.avif';
import baliImage from './images/bali.h';
import goaImage from './images/goaa.jpg';
import PondicherryImage from './images/Pondicherry.jpg';
import './Carousel.css';

function Carousel() {
    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 2,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    initialSlide: 1
                }
            }
        ]
    };

    return (
        <div style={{ width: '75%', margin: 'auto' }}>
            <div style={{ marginTop: '20px' }}>
                <Slider {...settings}>
                    {data.map((d, index) => (
                        <div key={index} className='carousel-item' style={{ display: 'inline-block', margin: '0 10px' }}>
                            <div className='carousel-item-content' style={{ backgroundColor: 'white', height: '500px', color: 'black', borderRadius: '10px', boxShadow: '0 4px 6px rgba(0, 0, 0, 0.1)' }}>
                                <div className='carousel-item-image' style={{ height: '250px', display: 'flex', justifyContent: 'center', alignItems: 'center', backgroundColor: 'indigo', borderRadius: '10px 10px 0 0' }}>
                                    <img src={d.img} alt={d.name} style={{ height: '70%', width: '60%', borderRadius: '50%', justifyContent: 'space-around' }} />
                                </div>
                                <div className="carousel-item-text" style={{ padding: '20px' }}>
                                    <p style={{ fontSize: '20px', fontWeight: 'bold' }}>{d.name}</p>
                                    <p>{d.review}</p>
                                    <button style={{ backgroundColor: 'black', color: 'pink', fontSize: '16px', padding: '8px 20px', borderRadius: '5px', border: 'none', cursor: 'pointer' }}>Read More</button>
                                </div>
                            </div>
                        </div>
                    ))}
                </Slider>
            </div>
        </div>
    );
}

const data = [
    {
        name: 'Kerala',
        img: keralaImage,
        review: 'I have not personally been to Kerala Place, but it is a popular restaurant known for its authentic Kerala cuisine. You might want to check out online reviews to get a better idea of what people think.'
    },
    {
        name: 'Manali',
        img: manaliImage,
        review: 'I have not personally been to Kerala Place, but it is a popular restaurant known for its authentic Kerala cuisine. You might want to check out online reviews to get a better idea of what people think.'
    },
    {
        name: 'Ladakh',
        img: ladakhImage,
        review: 'I have not personally been to Kerala Place, but it is a popular restaurant known for its authentic Kerala cuisine. You might want to check out online reviews to get a better idea of what people think.'
    },
    {
        name: 'Bali',
        img: baliImage,
        review: 'I have not personally been to Kerala Place, but it is a popular restaurant known for its authentic Kerala cuisine. You might want to check out online reviews to get a better idea of what people think.'
    },
    {
        name: 'Goa',
        img: goaImage,
        review: 'I have not personally been to Kerala Place, but it is a popular restaurant known for its authentic Kerala cuisine. You might want to check out online reviews to get a better idea of what people think.'
    },
    {
        name: 'Pondicherry',
        img: PondicherryImage,
        review: 'I have not personally been to Kerala Place, but it is a popular restaurant known for its authentic Kerala cuisine. You might want to check out online reviews to get a better idea of what people think.'
    }
];

export default Carousel;
 