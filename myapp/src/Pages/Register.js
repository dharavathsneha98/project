import React, { useState } from 'react';
import { Container, Form, Row, Col, FormGroup, Input, Label, Button } from 'reactstrap';
import axios from 'axios';
import './register.css'
import { useNavigate } from 'react-router';

function Register() {
  let navigate=useNavigate()

  const [formData, setFormData] = useState({
    email: '',
    password: '',
    address: '',
    address2: '',
    city: '',
    state: '',
    zip: '',
    check: ''
  });

  const handleOnChange = (e) => {
    const { name, value, type, checked } = e.target;
    const inputValue = type === 'checkbox' ? checked : value;
   
    setFormData({
      ...formData,
      [name]: inputValue
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(formData);

    const register = async () => {
      try {
        let response=await axios.post("http://localhost:5009/posting33",formData);
        console.log(response);
        if(response.data.ok === 1){
          alert("Registration Successfull")
          setFormData({
              email:'',
              password:'',
              address:'',
              address2:'',
              city:'',
              zip:'',
              check:'false',
              state:''
          
          })
          navigate('/login')
         }
      } catch(err) {
        console.log(err);
      }
    };

    register();
  };

  return (
    <Container>
      <div style={{ border: '1px solid black', padding: '3rem', borderRadius: '10px' }}>
        <Form>
          <Row>
            <Col md={6}>
              <FormGroup>
                <Label for="exampleEmail">Email</Label>
                <Input
                  id="exampleEmail"
                  name="email"
                  type="email"
                  value={formData.email}
                  onChange={handleOnChange}
                  placeholder="Enter your email"
                />
              </FormGroup>
            </Col>
            <Col md={6}>
              <FormGroup>
                <Label for="examplePassword">Password</Label>
                <Input
                  id="examplePassword"
                  name="password"
                  type="password"
                  value={formData.password}
                  onChange={handleOnChange}
                  placeholder="Enter your password"
                />
              </FormGroup>
            </Col>
          </Row>
          <FormGroup>
            <Label for="exampleAddress">Address</Label>
            <Input
              id="exampleAddress"
              name="address"
              value={formData.address}
              onChange={handleOnChange}
              placeholder="Enter your address"
            />
          </FormGroup>
          <FormGroup>
            <Label for="exampleAddress2">Address 2</Label>
            <Input
              id="exampleAddress2"
              name="address2"
              value={formData.address2}
              onChange={handleOnChange}
              placeholder="Apartment, studio, or floor"
            />
          </FormGroup>
          <Row>
            <Col md={6}>
              <FormGroup>
                <Label for="exampleCity">City</Label>
                <Input
                  id="exampleCity"
                  name="city"
                  value={formData.city}
                  onChange={handleOnChange}
                  placeholder="Enter your city"
                />
              </FormGroup>
            </Col>
            <Col md={3}>
              <FormGroup>
                <Label for="exampleState">State</Label>
                <Input
                  id="exampleState"
                  name="state"
                  value={formData.state}
                  onChange={handleOnChange}
                  placeholder="Enter your state"
                />
              </FormGroup>
            </Col>
            <Col md={3}>
              <FormGroup>
                <Label for="exampleZip">Zip</Label>
                <Input
                  id="exampleZip"
                  name="zip"
                  value={formData.zip}
                  onChange={handleOnChange}
                  placeholder="Enter your zip code"
                />
              </FormGroup>
            </Col>
          </Row>
          <FormGroup check>
            <Input
              id="exampleCheck"
              name="check"
              type="checkbox"
              value={formData.check}
              onChange={handleOnChange}
            />
            <Label check for="exampleCheck">Check me out</Label>
          </FormGroup>
          <Button onClick={handleSubmit}>Sign up</Button>
        </Form>
      </div>
    </Container>
  );
}

export default Register;
