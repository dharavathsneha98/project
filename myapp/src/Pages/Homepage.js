import React from 'react'
import Footer from '../Components/Footer'
import Carousel from './Carousel'
import ProductsList from '../Components/ProductsList'
import ProductMoreDetails from '../Components/ProductMoreDetails'

function Homepage() {
  return (
    <div>
      <Carousel />
      <div style={{padding:'40px', backgroundColor:'pink', textAlign:'justify', margin:'60px'}}>
    <h5> How about exploring the breathtaking beaches of Goa, experiencing the serene beauty of Manali, embarking on an adventurous journey to Ladakh, indulging in the lush greenery of Kerala, soaking up the French vibes in Pondicherry, and discovering the tropical paradise of Bali? These destinations will surely captivate the hearts of travelers seeking unforgettable experiences. Let Panchhi Bano be your guide to these incredible destinations! 🌴✈️🌞</h5>
    </div>
      <ProductsList />
      <ProductMoreDetails/>
      
   
    </div>
  )
}

export default Homepage
