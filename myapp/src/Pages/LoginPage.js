import React, { useState } from 'react';
import axios from 'axios';
import { Input, FormGroup, Label, Col, Form, Button } from 'reactstrap';

function LoginPage() {
    const [formdata, setFormdata] = useState({
        email: '',
        password: '',
        check: false
    });

    const handleOnChange = (e) => {
        const { name, value, type, checked } = e.target;
        const inputValue = type === 'checkbox' ? checked : value;
        setFormdata({
            ...formdata,
            [name]: inputValue
        });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        console.log(formdata);
        await login();
    };

    const login = async () => {
        try {
            const response = await axios.post("http://localhost:5007/login", formdata);
            console.log(response);
            alert("Login successful");
        } catch (err) {
            console.log(err);
        }
    };

    return (
        <div style={{ border: '1px solid gray', maxWidth: '500px', padding: '1rem', margin: '3rem auto' }}>
            <Form>
                <Col sm={12}>
                    <FormGroup>
                        <Label for="exampleEmail">Email</Label>
                        <Input
                            id="exampleEmail"
                            name="email"
                            placeholder="Enter email"
                            type="email"
                            value={formdata.email}
                            onChange={handleOnChange}
                        />
                    </FormGroup>
                </Col>
                <Col sm={12}>
                    <FormGroup>
                        <Label for="examplePassword">Password</Label>
                        <Input
                            id="examplePassword"
                            name="password"
                            placeholder="Enter password"
                            type="password"
                            value={formdata.password}
                            onChange={handleOnChange}
                        />
                    </FormGroup>
                </Col>
                <Col sm={12}>
                    <FormGroup check>
                        <Input
                            id="exampleCheck"
                            name="check"
                            type="checkbox"
                            value={formdata.check}
                            onChange={handleOnChange}
                        />
                        <Label check for="exampleCheck">Check me out</Label>
                    </FormGroup>
                </Col>
                <Col sm={12}>
                    <Button onClick={handleSubmit} type='submit'>Login</Button>
                </Col>
            </Form>
        </div>
    );
}

export default LoginPage;
