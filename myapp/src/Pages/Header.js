import React from 'react';
import { Link } from 'react-router-dom'; 
const Header = () => {
  return (
    <header className="bg-blue-200 p-4 shadow-md">
      <div className="container mx-auto flex justify-between items-center">
        <h1 className="text-white text-2xl font-bold">Panchibano</h1>
        <nav>
          <ul className="flex space-x-6">
            <li>
              <Link to="/" className="text-white hover:text-gray-300 font-semibold transition duration-300">Home</Link>
            </li>
            <li>
              <Link to="/tours" className="text-white hover:text-gray-300 font-semibold transition duration-300">Tours</Link>
            </li>
            <li>
              <Link to="/about" className="text-white hover:text-gray-300 font-semibold transition duration-300">About</Link>
            </li>
            <li>
              <Link to="/contact" className="text-white hover:text-gray-300 font-semibold transition duration-300">Contact</Link>
            </li>
          </ul>
        </nav>
      </div>
    </header>
  );
};

export default Header;
